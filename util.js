
global.random = function (array) {
	return array[Math.floor(Math.random() * array.length)]
}

global.sanitizeName = function sanitizeName(str){ // for showing names in discord
	str = str.replace(/[_~*\\]/g,"\\$&"); // formatting
	str = str.replace(/discord.gg\//g, 'discord.gg\\/'); // invites
	str = str.replace(/(http|https):\/\//g, "$1\\://"); // urls
	return str;
}
