var WebSocket = require('ws');
var Discord = require('discord.js');
var WebSocketMessageCollector = require('./lib/datacollector');

var webhook = new Discord.WebhookClient({url: config.webhooks.ddp}, {allowedMentions: {parse: []}});

var ws;
var wasConnected = false;
//var myId;

var wsc = new WebSocketMessageCollector(async function(data, startDate, endDate){
	await webhook.send({files:[{
		attachment: data,
		name: `daydun piano main raw data recording from ${startDate.toISOString()} to ${endDate.toISOString()} .txt.gz`
	}]});
});

(function connect() {
	ws = new WebSocket("wss://daydun.com:5012/?nick=%5Bdiscord.gg%2Fk44Eqha%5D");
	ws.on("open", () => {
		if (!wasConnected) send2discord("**Connected**");
		wasConnected = true;
	});
	ws.on("message", message => {
		wsc.collect(message);
		if (typeof message != 'string') return;
		var transmission = JSON.parse(message);
		if (transmission.type == 'chat') {
			let chatmsg = transmission.message;
			if (chatmsg.type == "message") {
				//if (chatmsg.id != myId)
				if (!chatmsg.content.startsWith('\u034f'))
					send2discord(`**${sanitizeName(chatmsg.user.nick)}:** ${chatmsg.content}`);
			} else if (chatmsg.type == "join") {
				send2discord(`__***${sanitizeName(chatmsg.nick || chatmsg.id)} joined.***__`);
			} else if (chatmsg.type == "leave") {
				send2discord(`__***${sanitizeName(chatmsg.nick || chatmsg.id)} left.***__`);
			}
		} /*else if (transmission.type == 'load') {
			myId = transmission.id;
		}*/
	});
	ws.on("error", error => handleError(error));
	ws.on("close", () => {
		if (wasConnected) send2discord("**Disconnected**");
		wasConnected = false;
		setTimeout(connect, 5000);
	});
})();

function send2discord(message) {
	webhook.send(message.substring(0,2000));
}

function send2ddp(message) {
	if (ws.readyState == WebSocket.OPEN) ws.send(JSON.stringify({type:"chat",message}));
}

dClient.on("messageCreate", message => {
	if (message.channel.id != "508890674138054667" || message.author.bot) return;
	var x = message.cleanContent;
	if (message.attachments.size > 0) x += ' ' + message.attachments.map(a => a.url).join(' ');
	send2ddp(`\u034f${message.member.displayName}#${message.author.discriminator}: ${x}`);
});
