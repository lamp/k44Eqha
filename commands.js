global.commands = [
	{
		name: "list",
		description: "Show the members on the other side of an MPP bridge",
		exec: i => {
			let bridge = bridges.find(x => x.channel == i.channel.id);
			if (!bridge) return i.reply({ephemeral: true, content: "Not available in this channel"});
			let ppl_list = Object.values(bridge.client.ppl).map(m => `\`${m._id}\` ${m.name}`);
			i.reply({content: `__**${ppl_list.length} people are playing**__\n${ppl_list.join("\n")}`});
		}
	}
];


dClient.on("interactionCreate", interaction => {
	commands.find(x => x.name == interaction.commandName)?.exec?.(interaction);
});

dClient.once("ready", () => {
	dClient.guilds.resolve(config.guildID)?.commands.set(commands);
});