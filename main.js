require("dotenv").config();
require('./util');
global.config = require('./config');
if (config.testmode) console.log('TEST MODE');
global.exitHook = require('async-exit-hook');
global.Discord = require('discord.js');
global.fs = require('fs');
global.dClient = new Discord.Client({
	intents: 32767,
	restRequestTimeout: 5*60*1000,
	allowedMentions: {parse: []}
});

// error handling
{
	let webhook = new Discord.WebhookClient({url: config.webhooks.error}, {allowedMentions: {parse: []}});
	global.handleError = function logError(error, title) {
		let msg = error && (error.stack || error.message || error);
		console.error(title + ':\n' + msg);
		try {
			webhook.send(`${title ? `**${title}:**` : ""}\`\`\`\n${msg}\n\`\`\``).catch(()=>{});
		} catch(e) {}
	}
	process.on('unhandledRejection', error => handleError(error, "Unhandled Rejection"));
	exitHook.uncaughtExceptionHandler(error => handleError(error, "Uncaught Exception"));
	dClient.on('error', error => handleError(error, "Discord Client Error"));
	dClient.on('warn', error => handleError(error, "Discord Client Warning"));

}

dClient.login(config.DISCORD_TOKEN);

dClient.on('ready', () => {
	console.log('Discord Client Ready');
});

require('./eval-exec');
require("./commands");
require('./mppbridger');
require('./misc');
require('./ddpbridge');
