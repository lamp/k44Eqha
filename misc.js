// join/leave
(async function(){
	var webhook = new Discord.WebhookClient({url: config.webhooks.welcome}, {allowedMentions: {parse: []}});
	dClient.on('guildMemberAdd', async member => {
		if (member.guild.id != config.guildID) return;
		let username = member.user.username.toLowerCase().includes('clyde') ? member.user.username.replace(/C/g,'Q').replace(/c/g,'q') : member.user.username;
		webhook.send({content: `${member} joined.`, username, avatarURL: member.user.displayAvatarURL({format:'png',size:2048})});
	});
	dClient.on('guildMemberRemove', async member => {
		if (member.guild.id != config.guildID) return;
		let username = member.user.username.toLowerCase().includes('clyde') ? member.user.username.replace(/C/g,'Q').replace(/c/g,'q') : member.user.username;
		webhook.send({content: `${member.user.tag} left.`, username, avatarURL: member.user.displayAvatarURL({format:'png',size:2048})});
	});
})();


// view deleted channels
(async function(){
	dClient.on('voiceStateUpdate', async (oldState, newState) => {
		if (oldState.channelId != config.channels.view_deleted_channels && newState.channelId == config.channels.view_deleted_channels) {
			// member joined the channel
			newState.member.roles.add(config.roles.viewing_deleted_channels);
		} else if (oldState.channelId == config.channels.view_deleted_channels && newState.channelId != config.channels.view_deleted_channels) {
			// member left the channel
			newState.member.roles.remove(config.roles.viewing_deleted_channels);
		}
	});
})();


// arrange bots at bottom of list
(async function(){
	let prefix = "\u17b5";
	let onNick = async member => {
		if (member.guild.id != config.guildID) return;
		if (member.user.bot && !member.displayName.startsWith(prefix))
			await member.setNickname(`${prefix}${member.displayName}`.substring(0,32));
	};
	dClient.on('guildMemberAdd', onNick);
	dClient.on('guildMemberUpdate', async (oldMember, newMember) => {
		if (newMember.displayName != oldMember.displayName) await onNick(newMember);
	});
})();


// persistent emojis
dClient.on("emojiDelete", async emoji => {
	console.log("emoji deleted:", emoji.name, emoji.url);
	if (global.disableEmojiProtection) return;
	if (emoji.name.toLowerCase().includes('delete')) return;
	async function readdEmoji() {
		await emoji.guild.emojis.create(emoji.url, emoji.name);
		delete readdEmoji;
	}
	// re-add emoji in 5 to 10 minutes
	setTimeout(() => {
		if (readdEmoji) readdEmoji();
	}, 300000 + Math.random() * 300000);
	// wouldn't want emoji to be lost if process is stopped before timeout ends
	exitHook(callback => {
		if (readdEmoji) readdEmoji().then(() => callback());
		else callback();
	});
});


// allow anyone to pin a message via reaction
// todo need enable partials for it to work on old messages
dClient.on("messageReactionAdd", async (messageReaction) => {
	if (messageReaction.guild?.id != config.guildID) return;
	if (messageReaction.emoji.name == "📌" || messageReaction.emoji.name == "📍")
		await messageReaction.message.pin();
});
