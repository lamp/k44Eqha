module.exports = class DiscordMessageSender {
	constructor(channel) {
		this.channel = channel;
		this.interval = setInterval(this.flush.bind(this), 3000);
	}

	//async init() {
	//	var webhooks = await this.channel.fetchWebhooks();
	//	this.webhook = webhooks.filter(w => w.token).first() || await this.channel.createWebhook("sdfadffg");
	//	this.interval = setInterval(this.flush.bind(this), 3000);
	//}


	send(text) {
		this.pp ||= {content: ""};
		this.pp.content += text + '\n';
	}

	sendEmbed(embed) {
		this.pp ||= {};
		this.pp.embeds = [embed];
		this.flush();
	}

	flush() {
		if (!this.pp) return;
		var pp = this.pp;
		delete this.pp;
		if (pp.content?.length > 2000) {
			pp.files = [{name: "message.txt", attachment: Buffer.from(pp.content)}];
			delete pp.content;
		}
		//this.webhook.send(pp).catch(error => {
			//handleError(error, "webhook");
			this.channel.send(pp);
		//});
	}

}
